document.addEventListener("DOMContentLoaded", () => {
	var selects2 = [];
  svg4everybody();
	var animateCSS = function(element, animation, time, prefix = 'animate__') {
		return new Promise((resolve, reject) => {
			var animationName = `${prefix}${animation}`;
			var node = document.querySelector(element);
			
			node.classList.add(`${prefix}animated`, animationName);

			function handleAnimationEnd(event) {
				event.stopPropagation();
				setTimeout(function() {
					node.classList.remove(`${prefix}animated`, animationName);
					resolve('Animation ended');
				}, time)
			}

			node.addEventListener('animationend', handleAnimationEnd, {once: true});
		});
	};
  (function() {
    var forms = document.querySelectorAll('.needs-validation');  
    validation = Array.prototype.filter.call(forms, function(form) {
      
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  })();
  (function() {
		var selects = document.querySelectorAll('[data-element="select"]');
		Array.prototype.filter.call(selects, function(select) {
			var placeholder = $(select).data('placeholder') ?? 'Выберите значение...';
			var dropdownCssClass = $(select).data('containerClass') ?? '';
			var dropdownParent = $(select).parent();
			
			$(select).select2({
				placeholder,
				dropdownCssClass,
				dropdownParent,
				minimumResultsForSearch: Infinity
			})
		});
  })();
	var swiper = new Swiper("[data-element='carousel--base']", {});
	var menuToggles = document.querySelectorAll('[data-element="menu-toggle"]');
	menuToggles.forEach(function(elem) {
		elem.addEventListener('click', function(e) {
			var _this = e.currentTarget;
			document.querySelector(`[data-id="${_this.dataset.target}"]`).classList.toggle('visible');
		})
	})
});